﻿using System;
using System.Collections.Generic;

namespace Sensormatic.AvgOppent.Worker.Models
{
    public class OppentLoginResponse
    {
        public OppentPayload payload { get; set; }
        public int retcode { get; set; }
    }

    public class OppentPayload
    {
        public string sessiontoken { get; set; }
        public OppentUser user { get; set; }
        public OppentAccount account { get; set; }
        public string username { get; set; }
    }

    public class OppentUser
    {
        public string firstname { get; set; }
        public string employeenr { get; set; }
        public string lastname { get; set; }
    }

    public class OppentRole
    {
        public string name { get; set; }
    }

    public class OppentAccount
    {
        public List<OppentRole> roles { get; set; }
        public DateTime validfrom { get; set; }
        public bool isvalid { get; set; }
        public string id { get; set; }
        public bool enabled { get; set; }
        public string username { get; set; }
        public DateTime validto { get; set; }
    }
}
