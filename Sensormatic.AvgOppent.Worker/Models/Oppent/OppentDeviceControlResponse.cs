﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensormatic.AvgOppent.Worker.Models
{
    public class OppentDeviceControlResponse
    {
        public OppentDeviceControlPayload payload { get; set; }
        public int retcode { get; set; }
    }

    public class OppentDeviceControlPayload
    {
        public List<OppentDeviceControlDevice> devices { get; set; }
    }

    public class OppentDeviceControlDevice
    {
        public string node { get; set; }
        public OppentDeviceControlMeta meta { get; set; }
        public string name { get; set; }
        public OppentDeviceControlLocation location { get; set; }
        public string type { get; set; }
        public string @class { get; set; }
        public bool enabled { get; set; }
    }

    public class OppentDeviceControlLocation
    {
        public List<double> coord { get; set; }
        public int course { get; set; }
        public OppentDeviceControlCurrentnode currentnode { get; set; }
        public string map { get; set; }
        public string group { get; set; }
    }

    public class OppentDeviceControlCurrentnode
    {
        public string name { get; set; }
        public int id { get; set; }
    }

    public class OppentDeviceControlMeta
    {
        public string driverid { get; set; }
        public string hardwareid { get; set; }
        public OppentDeviceControlCommandset commandset { get; set; }
        public OppentDeviceControlState2 state { get; set; }
        public string deviceid { get; set; }
        public string devicetype { get; set; }
    }

    public class OppentDeviceControlCommandset
    {
        public string version { get; set; }
        public List<OppentDeviceControlCommand> commands { get; set; }
        public string devicetype { get; set; }
    }

    public class OppentDeviceControlCommand
    {
        public OppentDeviceControlArgs args { get; set; }
        public string name { get; set; }
    }

    public class OppentDeviceControlArgs
    {
        public OppentDeviceControlValue value { get; set; }
    }

    public class OppentDeviceControlValue
    {
        public bool freevalue { get; set; }
        public bool fixedvalue { get; set; }
        public List<object> domain { get; set; }
    }

    public class OppentDeviceControlState2
    {
        public int value { get; set; }
        public bool isconnected { get; set; }
        public OppentDeviceControlState state { get; set; }
        public DateTime timestamp { get; set; }
    }

    public class OppentDeviceControlState
    {
        public int value { get; set; }
    }
}
