﻿using System;
using System.Collections.Generic;

namespace Sensormatic.AvgOppent.Worker.Models
{
    public class AvgEventResponse
    {
        public string status { get; set; }
        public AvgResult result { get; set; }
    }

    public class AvgResult
    {
        public List<AvgEvent> events { get; set; }
    }
    public class AvgEvent
    {
        public List<object> targetIdsDeprecated { get; set; }
        public List<AvgClassifiedObject> classifiedObjects { get; set; }
        public string analyticEventName { get; set; }
        public string area { get; set; }
        public string activity { get; set; }
        public DateTime eventTriggerTime { get; set; }
        public string cameraId { get; set; }
        public string thisId { get; set; }
        public string linkedEventId { get; set; }
        public DateTime timestamp { get; set; }
        public string originatingEventId { get; set; }
        public string originatingServerName { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public string originatingServerId { get; set; }
        public List<object> cameraIds { get; set; }
        public List<object> entityIds { get; set; }
    }
    public class AvgClassifiedObject
    {
        public string subclass { get; set; }
        public int objectId { get; set; }
    }
}
