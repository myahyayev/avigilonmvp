﻿namespace Sensormatic.AvgOppent.Worker.Models
{
    public class AvgLoginResponse
    {
        public string status { get; set; }
        public AvgSession result { get; set; }
    }
    public class AvgSession
    {
        public string session { get; set; }
        public string externalUserId { get; set; }
        public string domainId { get; set; }
    }
}
