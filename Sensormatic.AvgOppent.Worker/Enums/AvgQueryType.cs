﻿namespace Sensormatic.AvgOppent.Worker.Enums
{
    public enum AvgQueryType
    {
        ACTIVE,
        TIME_RANGE,
        CONTINUE
    }
}
