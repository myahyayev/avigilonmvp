﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Sensormatic.AvgOppent.Worker.Config;
using Sensormatic.AvgOppent.Worker.Constants;
using Sensormatic.AvgOppent.Worker.Extensions;
using Sensormatic.AvgOppent.Worker.Interfaces;
using Sensormatic.AvgOppent.Worker.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Sensormatic.AvgOppent.Worker.Services
{
    public class AvgOppentIntegrationService : IAvgOppentIntegrationService
    {
        private readonly IOptions<AvgConfig> _avgConfig;
        private readonly IOptions<OppentConfig> _oppentConfig;
        private readonly HttpClient _avgClient;
        private readonly HttpClient _oppentClient;
        private readonly ILogger<AvgOppentIntegrationService> _logger;
        //private readonly IMailService _mailService;
        public AvgOppentIntegrationService(ILogger<AvgOppentIntegrationService> logger,
                                           IOptions<AvgConfig> avgConfig,
                                           IOptions<OppentConfig> oppentConfig,
                                           IHttpClientFactory clientFactory
                                           //IMailService mailService
             )
        {
            _logger = logger;
            _avgConfig = avgConfig;
            _oppentConfig = oppentConfig;

            _avgClient = clientFactory.CreateClient("avg");
            _avgClient.BaseAddress = new Uri(avgConfig.Value.BaseUrl);

            _oppentClient = clientFactory.CreateClient("oppent");
            _oppentClient.BaseAddress = new Uri(oppentConfig.Value.BaseUrl);

           // _mailService = mailService;
        }

        public async Task<bool> RunAsync()
        {
            var eventResponse = await AvgGetEventsAsync();

            if (eventResponse != null && eventResponse.status == "success")
            {
                _logger.LogInformation($"Total Event Count: {eventResponse.result.events.Count}");

                var events = eventResponse.result.events
                          .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                          .ToList();

                _logger.LogInformation($"Grouped Event Count: {events.Count}");

                //foreach (var item in events)
                //{
                //    _logger.LogInformation($"Grouped: {item.analyticEventName}   {item.type} ");
                //}
               


                if (events.Count > 0)
                {
                    var oppentLoginResponse = await OppentLoginAsync();

                    if (oppentLoginResponse != null && oppentLoginResponse.payload != null && !string.IsNullOrEmpty(oppentLoginResponse.payload.sessiontoken))
                    {
                        foreach (var e in events.Where(u => u.type == "DEVICE_ANALYTICS_STOP"))
                        {
                            _logger.LogInformation($"{e.analyticEventName}  type: DEVICE_ANALYTICS_STOP");

                            var oppentDeviceControlResponseInput =
                                await OppentDeviceControlAsync(e.analyticEventName, "input", oppentLoginResponse.payload.sessiontoken);

                            if (oppentDeviceControlResponseInput != null && oppentDeviceControlResponseInput.payload != null && oppentDeviceControlResponseInput.payload.devices != null)
                            {
                                var deviceInput = oppentDeviceControlResponseInput.payload.devices.FirstOrDefault();

                                if (deviceInput != null && deviceInput.meta.state.state.value == 0)
                                {
                                    var oppentWriteAttResponse = await OppentWriteAttAsync("1", e.analyticEventName, oppentLoginResponse.payload.sessiontoken);
                                    if (oppentWriteAttResponse == null || oppentWriteAttResponse.payload == null || oppentWriteAttResponse.payload.state == null || oppentWriteAttResponse.payload.state.state == null)
                                    {
                                        _logger.LogInformation($"Input -->> OppentWrite  AlarmType: DEVICE_ANALYTICS_STOP,   ZoneName: {e.analyticEventName}, Posted Value: 1,      Result:  value is null");
                                    }
                                    else
                                    {
                                        _logger.LogInformation($"Input -->> OppentWrite  AlarmType: DEVICE_ANALYTICS_STOP,  ZoneName: {e.analyticEventName}, Posted Value: 1,     Result:  {oppentWriteAttResponse.payload.state.state.value}");
                                    }
                                }

                                else
                                {
                                    _logger.LogInformation($" DEVICE_ANALYTICS_STOP    deviceInput.meta.state.state.value == 0 'dan faklidir   {deviceInput.name}   {e.analyticEventName}");
                                }
                            }
                            else
                            {
                                _logger.LogInformation($"Output -->> OppentDeviceControl AlarmType: DEVICE_ANALYTICS_STOP,   ZoneName: {e.analyticEventName},    Result:  value is null");
                            }
                        }





                        foreach (var e in events.Where(u => u.type == "DEVICE_ANALYTICS_START"))
                        {
                            _logger.LogInformation($"{e.analyticEventName}  type: DEVICE_ANALYTICS_START");

                            var oppentDeviceControlResponseOutput =
                                await OppentDeviceControlAsync(e.analyticEventName, "output", oppentLoginResponse.payload.sessiontoken);

                            if (oppentDeviceControlResponseOutput != null && oppentDeviceControlResponseOutput.payload != null && oppentDeviceControlResponseOutput.payload.devices != null)
                            {
                                var deviceOutput = oppentDeviceControlResponseOutput.payload.devices.FirstOrDefault();

                                if (deviceOutput != null && deviceOutput.meta.state.state.value == 0)
                                {
                                    var oppentDeviceControlResponseInput =
                                               await OppentDeviceControlAsync(e.analyticEventName, "input", oppentLoginResponse.payload.sessiontoken);

                                    if (oppentDeviceControlResponseInput != null && oppentDeviceControlResponseInput.payload != null && oppentDeviceControlResponseInput.payload.devices != null)
                                    {
                                        var deviceInput = oppentDeviceControlResponseInput.payload.devices.FirstOrDefault();

                                        if (deviceInput != null && deviceInput.meta.state.state.value == 1)
                                        {
                                            var oppentWriteAttResponse = await OppentWriteAttAsync("0", e.analyticEventName, oppentLoginResponse.payload.sessiontoken);

                                            if (oppentWriteAttResponse == null || oppentWriteAttResponse.payload == null || oppentWriteAttResponse.payload.state == null || oppentWriteAttResponse.payload.state.state == null)
                                                _logger.LogWarning($"Input -->> OppentWrite  AlarmType: DEVICE_ANALYTICS_START,   ZoneName: {e.analyticEventName}, Posted Value: 0,   Result:  value is null");

                                            else
                                                _logger.LogInformation($"Input -->> OppentWrite  AlarmType: DEVICE_ANALYTICS_START,  ZoneName: {e.analyticEventName}, Posted Value: 0,   Result:  {oppentWriteAttResponse.payload.state.state.value}");
                                        }
                                        else
                                        {
                                            _logger.LogInformation($"DEVICE_ANALYTICS_START   deviceInput.meta.state.state.value == 1 'dan faklidir   {deviceInput.name}   {e.analyticEventName}");
                                        }
                                    }
                                    else
                                    {
                                        _logger.LogInformation($"Input -->> OppentDeviceControl AlarmType: DEVICE_ANALYTICS_START,   ZoneName: {e.analyticEventName},    Result:  value is null");
                                    }
                                }
                            }
                            else
                            {
                                _logger.LogInformation($"Output -->> OppentDeviceControl AlarmType: DEVICE_ANALYTICS_START,   ZoneName: {e.analyticEventName},    Result:  value is null");
                            }
                        }
                    }
                    else
                    {
                        _logger.LogInformation("Oppent tarafindan token alinamadi");
                    }
                }
                {
                    _logger.LogInformation("Agv tarafindan event listesi bostur");
                }
            }
            else
            {
                _logger.LogInformation("Agv tarafindan event listesi alinamadi!");
            }

            return true;
        }

        #region private methods
        private async Task<AvgLoginResponse> AvgLoginAsync()
        {
            AvgLoginResponse loginResonse = null;

            try
            {
                string authToken = Extension.GenerateAuthToken(_avgConfig.Value.UserNonce, _avgConfig.Value.UserKey);

                var reqesut = new AvgLoginRequest
                {
                    authorizationToken = authToken,
                    clientName = _avgConfig.Value.ClientName,
                    password = _avgConfig.Value.Password,
                    siteName = _avgConfig.Value.SiteName,
                    username = _avgConfig.Value.UserName
                };

                var httpContent = new StringContent(JsonConvert.SerializeObject(reqesut), Encoding.UTF8, "application/json");

                var response = await _avgClient.PostAsync("login", httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    loginResonse = JsonConvert.DeserializeObject<AvgLoginResponse>(result);
                   // _logger.LogInformation("AvgLoginAsync " + loginResonse.result.session);
                }

                else
                {
                    throw new Exception($"{response.StatusCode} : {result} ");
                }
            }
            catch (Exception ex)
            {
                //await _mailService.SendAsync(new MailRequest
                //{
                //    IsHtml = false,
                //    Subject = "Avg taraında login işlemi sırasında hata oluştu",
                //    Body = ex.Message,
                //});
                _logger.LogInformation("AvgLoginAsync " + ex.Message);
            }

            return loginResonse;
        }

        private async Task<AvgEventResponse> AvgGetEventsAsync()
        {
            AvgEventResponse eventResponse = null;

            try
            {
                var now = DateTime.UtcNow;
                var loginResponse = await AvgLoginAsync();

                if (loginResponse != null && loginResponse.status == "success")
                {

                    var builder = new UriBuilder(_avgConfig.Value.BaseUrl + "events/search?");

                    var query = HttpUtility.ParseQueryString(builder.Query);

                    string from  = now.AddMinutes(-10).ToString("s") + "Z";
                    string to = now.ToString("s") + "Z";
                    query["session"] = loginResponse.result.session;
                    query["queryType"] = AvgConstantParameters.QueryType;
                    query["serverId"] = _avgConfig.Value.ServerId;
                    query["eventTopics"] = JsonConvert.SerializeObject(AvgConstantParameters.EventTopincs);
                    query["from"] = from;
                    query["to"] = to;


                    _logger.LogInformation($"FromDate: {from}   to: {to} ");

                    builder.Query = query.ToString();
                    string queryUrl = builder.ToString();

                    _avgClient.DefaultRequestHeaders.Clear();

                    _avgClient.DefaultRequestHeaders.Add("x-avg-session", loginResponse.result.session);

                    var response = await _avgClient.GetAsync(queryUrl);

                    var result = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        eventResponse = JsonConvert.DeserializeObject<AvgEventResponse>(result);

                        //var events = eventResponse.result.events
                        // .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                        // .ToList();
                        //_logger.LogInformation("AvgGetEventsAsync " + events.Count);
                        
                    }

                    else
                        throw new Exception($"{response.StatusCode} : {result} ");
                }
            }
            catch (Exception ex)
            {
                //await _mailService.SendAsync(new MailRequest
                //{
                //    IsHtml = false,
                //    Subject = "Avg taraında event listesi alınırken hata oluştu",
                //    Body = ex.Message,
                //}); 

                _logger.LogInformation(ex.Message);
            }

            return eventResponse;
        }

        private async Task<OppentLoginResponse> OppentLoginAsync()
        {
            OppentLoginResponse oppentLoginResponse = null;

            try
            {
                var builder = new UriBuilder(_oppentConfig.Value.BaseUrl + "monitor/session/login?");

                var query = HttpUtility.ParseQueryString(builder.Query);

                query["username"] = _oppentConfig.Value.UserName;
                query["pwd"] = _oppentConfig.Value.Password;

                builder.Query = query.ToString();
                string queryUrl = builder.ToString();

                var response = await _oppentClient.GetAsync(queryUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    oppentLoginResponse = JsonConvert.DeserializeObject<OppentLoginResponse>(result);

                    //_logger.LogInformation("OppentLoginAsync " + oppentLoginResponse.payload.sessiontoken);

                }
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }
            catch (Exception ex)
            {
                //await _mailService.SendAsync(new MailRequest
                //{
                //    IsHtml = false,
                //    Subject = "Oppent tarafında login işlemi sırasında hata oluştu",
                //    Body = ex.Message
                //});
                _logger.LogInformation("OppentLoginAsync " + ex.Message);
            }

            return oppentLoginResponse;
        }

        private async Task<OppentWriteAttResponse> OppentWriteAttAsync(string value, string analyticEventName, string sessionToken)
        {
            OppentWriteAttResponse oppentWriteAttResponse = null;

            try
            {
                var request = new OppentWriteAttRequest()
                {
                    command = new OppentCommand
                    {
                        args = new OppentArgs() { value = value },
                        name = "write"
                    }
                };

                var httpContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

                var response = await _oppentClient.PostAsync($"{_oppentConfig.Value.BaseUrl}rest/devices/{analyticEventName}-input/command?&sessiontoken={sessionToken}", httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentWriteAttResponse = JsonConvert.DeserializeObject<OppentWriteAttResponse>(result);

                else
                    throw new Exception($"{response.StatusCode} : {result} ");

            }
            catch (Exception ex)
            {
                //await _mailService.SendAsync(new MailRequest
                //{
                //    IsHtml = false,
                //    Subject = "Oppent tarafında write işlemi sırasında hata oluştu",
                //    Body = ex.Message
                //});
                _logger.LogCritical(ex.Message);
            }

            return oppentWriteAttResponse;
        }

        private async Task<OppentDeviceControlResponse> OppentDeviceControlAsync(string analyticEventName, string inputOrOutput, string sessionToken)
        {
            OppentDeviceControlResponse oppentDeviceControlResponse = null;

            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync($"{_oppentConfig.Value.BaseUrl}rest/devices/{analyticEventName}-{inputOrOutput}?&sessiontoken={sessionToken}");

                    var result = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        oppentDeviceControlResponse = JsonConvert.DeserializeObject<OppentDeviceControlResponse>(result);
                    }
                    else
                        throw new Exception($"{response.StatusCode} : {result} ");
                }
            }
            catch (Exception ex)
            {
                //await _mailService.SendAsync(new MailRequest
                //{
                //    IsHtml = false,
                //    Subject = "Oppent tarafında DeviceControl işlemi sırasında hata oluştu",
                //    Body = ex.Message
                //});
                _logger.LogInformation(ex.Message);
            }

            return oppentDeviceControlResponse;
        }
        #endregion private methods
    }
}
