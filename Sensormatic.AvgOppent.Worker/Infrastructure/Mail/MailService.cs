﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using Sensormatic.AvgOppent.Worker.Config;
using System.IO;
using System.Threading.Tasks;

namespace Sensormatic.AvgOppent.Worker.Infrastructure
{
    public class MailService : IMailService
    {
        private readonly IOptions<MailConfig> _mailConfig;
        private SmtpClient _smtpClient;
        private readonly ILogger<MailService> _logger;
        public MailService(IOptions<MailConfig> mailConfig, ILogger<MailService> logger)
        {
            _logger = logger;
            _mailConfig = mailConfig;

            _smtpClient = new SmtpClient();
            _smtpClient.Connect(_mailConfig.Value.SMTPServer, _mailConfig.Value.SMTPPort);           
        }

        public async Task SendAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage
            {
                Sender = MailboxAddress.Parse(_mailConfig.Value.SMTPMailFrom)
            };

            _mailConfig.Value.SMTPTo.ForEach(x => email.To.Add(MailboxAddress.Parse(x)));

            email.Subject = mailRequest.Subject;

            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }

            if (mailRequest.IsHtml)
                builder.HtmlBody = mailRequest.Body;
            else
                builder.TextBody = mailRequest.Body;

            email.Body = builder.ToMessageBody();

            if (!_smtpClient.IsConnected)
            {
                _smtpClient.Connect(_mailConfig.Value.SMTPServer, _mailConfig.Value.SMTPPort);
            }

            await _smtpClient.SendAsync(email);

            _logger.LogInformation($"Email successfully send to {{EmailAddresses}} Email: {{EmailBody}}", string.Join(',', mailRequest.To), mailRequest.Body);
        }

        ~MailService()
        {
            _smtpClient.Disconnect(true);
        }
    }
}
