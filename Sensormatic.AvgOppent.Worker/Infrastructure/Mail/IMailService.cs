﻿using System.Threading.Tasks;

namespace Sensormatic.AvgOppent.Worker.Infrastructure
{
    public interface IMailService
    {
        Task SendAsync(MailRequest mailRequest);
    }
}
