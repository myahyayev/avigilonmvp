﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Sensormatic.AvgOppent.Worker.Infrastructure
{
    public class MailRequest
    {
        public List<string> To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<IFormFile> Attachments { get; set; }
        public bool IsHtml { get; set; }
    }
}
