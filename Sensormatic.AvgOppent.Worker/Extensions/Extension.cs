﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Sensormatic.AvgOppent.Worker.Extensions
{
    public class Extension
    {
        public static string GenerateAuthToken(string userNonce, string userKey)
        {
            using var crypt = new SHA256Managed();

            var timeStamp = (DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000 | 0).ToString();

            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(timeStamp + userKey));

            var stringBuilder = new StringBuilder();

            foreach (byte theByte in crypto)
                stringBuilder.Append(theByte.ToString("x2"));

            return $"{userNonce}:{ timeStamp}:{ stringBuilder.ToString()}";
        }
    }
}
