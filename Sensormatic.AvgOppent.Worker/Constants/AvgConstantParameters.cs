﻿using Sensormatic.AvgOppent.Worker.Enums;
using System.Collections.Generic;

namespace Sensormatic.AvgOppent.Worker.Constants
{
    public static class AvgConstantParameters
    {
        public static List<string> EventTopincs = new List<string> { "DEVICE_ANALYTICS_START", "DEVICE_ANALYTICS_STOP" };
        public static string QueryType = AvgQueryType.TIME_RANGE.ToString();
    }
}
