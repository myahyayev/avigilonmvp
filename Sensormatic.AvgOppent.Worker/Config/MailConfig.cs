﻿using System.Collections.Generic;

namespace Sensormatic.AvgOppent.Worker.Config
{
    public class MailConfig
    {
        public string SMTPServer { get; set; }
        public int SMTPPort { get; set; }
        public string SMTPMailFrom { get; set; }
        public string SMTPPasswordFrom { get; set; }
        public List<string> SMTPTo { get; set; }
    }
}
