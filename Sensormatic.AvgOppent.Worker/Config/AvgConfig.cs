﻿namespace Sensormatic.AvgOppent.Worker.Config
{
    public class AvgConfig
    {
       // public string LoginUrl { get; set; }
       // public string EventUrl { get; set; }
        public string UserNonce { get; set; }
        public string UserKey { get; set; }
        public string ClientName { get; set; }
        public string Password { get; set; }
        public string SiteName { get; set; }
        public string UserName { get; set; }
        public string ServerId { get; set; }
        public string BaseUrl { get; set; }
    }
}
