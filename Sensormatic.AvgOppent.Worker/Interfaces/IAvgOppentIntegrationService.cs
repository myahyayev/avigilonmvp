﻿using System.Threading.Tasks;

namespace Sensormatic.AvgOppent.Worker.Interfaces
{
    public interface IAvgOppentIntegrationService
    {
        Task<bool> RunAsync();
    }
}
