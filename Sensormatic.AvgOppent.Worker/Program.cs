using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sensormatic.AvgOppent.Worker.Config;
using Sensormatic.AvgOppent.Worker.Infrastructure;
using Sensormatic.AvgOppent.Worker.Interfaces;
using Sensormatic.AvgOppent.Worker.Services;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.IO;
using System.Net.Http;

namespace Sensormatic.AvgOppent.Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                .ConfigureHostConfiguration(hostConfig =>
                {
                    Console.WriteLine( Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT"));

                    var environment = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");
                    hostConfig.SetBasePath(Directory.GetCurrentDirectory());
                    hostConfig.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                    hostConfig.AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true);
                    hostConfig.AddEnvironmentVariables();
                    hostConfig.Build();
                })

                .UseWindowsService(options =>
                {
                    options.ServiceName = "PasslogicAvgOppentIntr";
                })

                .UseSerilog((context, loggerConfiguration) =>
                    loggerConfiguration
                        .ReadFrom.Configuration(context.Configuration)
                        .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Code)
                )

                .ConfigureAppConfiguration(builder =>
                 {
                     var configuration = builder.Build();
                 })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<AvgConfig>(hostContext.Configuration.GetSection("Avg"));
                    services.Configure<OppentConfig>(hostContext.Configuration.GetSection("Oppent"));
                    services.Configure<MailConfig>(hostContext.Configuration.GetSection("Mail"));
                    //services.AddSingleton<IMailService, MailService>();
                    services.AddSingleton<IAvgOppentIntegrationService, AvgOppentIntegrationService>();
                    services.AddHttpClient("avg")
                       .ConfigurePrimaryHttpMessageHandler(
                            () =>
                            {
                                var clientHandler = new HttpClientHandler();
                                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                                return clientHandler;
                            });
                    services.AddHttpClient("oppent");
                    services.AddHostedService<AvgOppentWorker>();
                });
    }
}
