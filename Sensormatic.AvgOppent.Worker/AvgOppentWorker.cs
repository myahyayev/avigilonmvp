using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sensormatic.AvgOppent.Worker.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sensormatic.AvgOppent.Worker
{
    public class AvgOppentWorker : BackgroundService
    {
        private readonly IAvgOppentIntegrationService _avgOppentIntegrationService;
        private readonly ILogger<AvgOppentWorker> _logger;

        public AvgOppentWorker(ILogger<AvgOppentWorker> logger, IAvgOppentIntegrationService avgOppentIntegrationService)
        {
            _logger = logger;
            _avgOppentIntegrationService = avgOppentIntegrationService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
          

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("AvgOppentWorker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(100, stoppingToken);
                try
                {
                    await _avgOppentIntegrationService.RunAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogCritical(ex.Message);
                }
                _logger.LogInformation("AvgOppentWorker stopped at: {time}", DateTimeOffset.Now);
            }
        }
    }
}
