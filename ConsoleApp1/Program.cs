﻿using ConsoleApp1.Models.OppentDeviceControl;
using ConsoleApp1.Models.OppentWriteAtt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ConsoleApp1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {

                var d =  Resources.TextFile1;

                var obj  = JsonConvert.DeserializeObject<OppentWriteAttResponse>(d);



                string authToken = Program.GenerateAuthToken();

                Console.WriteLine("Adim 1   " + authToken);

                //login
                string loginUrl = "https://localhost:8443/mt/api/rest/v1/login";   //"https://192.168.87.65:8443/mt/api/rest/v1/login"
                var loginResponse = await Program.AvgLoginAsync(loginUrl, authToken);

                Console.WriteLine("Adim 2   " + loginResponse.result.session);


                //getEvent
                List<string> eventTopincs = new List<string> { "DEVICE_ANALYTICS_START", "DEVICE_ANALYTICS_STOP" };
                string eventUrl = "https://localhost:8443/mt/api/rest/v1/events/search?";
                var eventResponse = await Program.AvgGetEventsAsync(eventUrl, loginResponse.result.session, eventTopincs);

                Console.WriteLine("Adim 3   eventResponse listesi alindi");

                if (eventResponse != null && eventResponse.status == "success")
                {
                    Console.WriteLine(eventResponse.result.events.Count);

                    var updatedEvents = eventResponse.result.events
                        .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                        .ToList();


                    string userName = "admin";
                    string password = "123456";
                    string oppentLoginUrl = "http://localhost:8081/wms/monitor/session/login?";

                    var oppentLoginResponse = await Program.OppentLoginAsync(oppentLoginUrl, userName, password);


                    foreach (var e in updatedEvents.Where(u => u.type == "DEVICE_ANALYTICS_STOP")) //stoplar icin 
                    {
                        string oppentDeviceUrl = $"http://localhost:8081/wms/rest/devices/{e.analyticEventName}-input/command?&sessiontoken={oppentLoginResponse.payload.sessiontoken}";

                        var oppentWriteAttResponse = await Program.OppentOppentWriteAttAsync(oppentDeviceUrl, 0);

                        Console.WriteLine(oppentWriteAttResponse.payload.state.state.value);
                    }


                    foreach (var e in updatedEvents.Where(u => u.type == "DEVICE_ANALYTICS_START"))
                    {
                        string urlDeviceControl = "http://localhost:8081/wms/rest/devices/{e.analyticEventName}-output?&sessiontoken={oppentLoginResponse.payload.sessiontoken}";

                        var oppentDeviceControlResponse = await Program.OppentDeviceControlAsync(urlDeviceControl);

                        if (oppentDeviceControlResponse.payload.devices != null)
                        {
                            var qres = oppentDeviceControlResponse.payload.devices.First().meta.state.state.value;

                            if (qres == 0)
                            {
                                string oppentDeviceUrl = $"http://localhost:8081/wms/rest/devices/{e.analyticEventName}-input/command?&sessiontoken={oppentLoginResponse.payload.sessiontoken}";
                                var oppentWriteAttResponse = await Program.OppentOppentWriteAttAsync(oppentDeviceUrl, 1);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            Console.ReadKey();
        }

        private static string GenerateAuthToken()
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var timeStamp = (DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000 | 0).ToString();

            var stringBuilder = new StringBuilder();

            string userNonce = "0014y00002UeGMgAAN";
            string userKey = "18a17222eb6ec766dba0378021dd91d947bf2d371383b0e9f36c653c8438411b";

            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(timeStamp + userKey));

            foreach (byte theByte in crypto)
                stringBuilder.Append(theByte.ToString("x2"));

            return $"{userNonce}:{ timeStamp}:{ stringBuilder.ToString()}";
        }

        private static async Task<AvgLoginResonse> AvgLoginAsync(string loginUrl, string authToken)
        {
            AvgLoginResonse loginResonse;

            var reqesut = new AvgLoginRequest
            {
                authorizationToken = authToken,
                clientName = "AvigilonAlarmDemoApp.UI",
                //password = "Ex1234xx",
                password = "Q1w2e3r4t5",
                siteName = "TRKESL0012",
                //siteName = "EXROOM02",
                username = "administrator"
            };

            var clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            {
                var httpContent = new StringContent(JsonConvert.SerializeObject(reqesut), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(loginUrl, httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    loginResonse = JsonConvert.DeserializeObject<AvgLoginResonse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return loginResonse;
        }

        private static async Task<AvgEventResponse> AvgGetEventsAsync(string eventUrl, string session, List<string> eventTopincs)
        {
            AvgEventResponse eventResponse;

            HttpClientHandler clientHandler = new HttpClientHandler();

            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            {
                var json = JsonConvert.SerializeObject(eventTopincs);
                var builder = new UriBuilder(eventUrl);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["session"] = session;
                query["queryType"] = AvgQueryType.TIME_RANGE.ToString();
                query["serverId"] = "H4F-HdtZSf6hdWVvnD_j7Q";
                query["eventTopics"] = json;

                var now = DateTime.UtcNow;
                query["from"] = now.AddDays(-1).ToString("s") + "Z";
                query["to"] = now.ToString("s") + "Z";

                //query["from"] = "2020-01-27T19:00:00.000Z";
                //query["to"] = "2022-01-27T19:00:00.000Z";

                builder.Query = query.ToString();
                string queryUrl = builder.ToString();

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("x-avg-session", session);

                var response = await client.GetAsync(queryUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    eventResponse = JsonConvert.DeserializeObject<AvgEventResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return eventResponse;
        }

        private static async Task<OppentLoginResponse> OppentLoginAsync(string loginUrl, string userName, string password)
        {
            OppentLoginResponse oppentLoginResponse;

            using (var client = new HttpClient())
            {
                var builder = new UriBuilder(loginUrl);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["username"] = userName;
                query["pwd"] = password;

                builder.Query = query.ToString();
                string queryUrl = builder.ToString();

                var response = await client.GetAsync(queryUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentLoginResponse = JsonConvert.DeserializeObject<OppentLoginResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return oppentLoginResponse;
        }

        private static async Task<OppentWriteAttResponse> OppentOppentWriteAttAsync(string eventUrl, int value)
        {
            OppentWriteAttResponse oppentWriteAttResponse;

            var request = new OppentWriteAttRequest()
            {
                command = new OppentCommand
                {
                    args = new OppentArgs()
                    {
                        value = value.ToString()
                    },
                    name = "write"
                }
            };

            using (var client = new HttpClient())
            {
                var httpContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(eventUrl, httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentWriteAttResponse = JsonConvert.DeserializeObject<OppentWriteAttResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return oppentWriteAttResponse;
        }

        private static async Task<OppentDeviceControlResponse> OppentDeviceControlAsync(string apiUrl)
        {
            OppentDeviceControlResponse oppentDeviceControlResponse;

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(apiUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentDeviceControlResponse = JsonConvert.DeserializeObject<OppentDeviceControlResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return oppentDeviceControlResponse;
        }
    }




    public class AvgLoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string authorizationToken { get; set; }
        public string clientName { get; set; }
        public string siteName { get; set; }
    }

    public class AvgLoginResponse
    {
        public string username { get; set; }
        public string password { get; set; }
        public string authorizationToken { get; set; }
        public string clientName { get; set; }
        public string siteName { get; set; }
    }

    public class AvgSession
    {
        public string session { get; set; }
        public string externalUserId { get; set; }
        public string domainId { get; set; }
    }

    public class AvgLoginResonse
    {
        public string status { get; set; }
        public AvgSession result { get; set; }
    }

    public enum AvgQueryType
    {
        ACTIVE,
        TIME_RANGE,
        CONTINUE
    }

    public class AvgClassifiedObject
    {
        public string subclass { get; set; }
        public int objectId { get; set; }
    }

    public class AvgEvent
    {
        public List<object> targetIdsDeprecated { get; set; }
        public List<AvgClassifiedObject> classifiedObjects { get; set; }
        public string analyticEventName { get; set; }
        public string area { get; set; }
        public string activity { get; set; }
        public DateTime eventTriggerTime { get; set; }
        public string cameraId { get; set; }
        public string thisId { get; set; }
        public string linkedEventId { get; set; }
        public DateTime timestamp { get; set; }
        public string originatingEventId { get; set; }
        public string originatingServerName { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public string originatingServerId { get; set; }
        public List<object> cameraIds { get; set; }
        public List<object> entityIds { get; set; }
    }

    public class AvgResult
    {
        public List<AvgEvent> events { get; set; }
    }

    public class AvgEventResponse
    {
        public string status { get; set; }
        public AvgResult result { get; set; }
    }



    public class OppentUser
    {
        public string firstname { get; set; }
        public string employeenr { get; set; }
        public string lastname { get; set; }
    }

    public class OppentRole
    {
        public string name { get; set; }
    }

    public class OppentAccount
    {
        public List<OppentRole> roles { get; set; }
        public DateTime validfrom { get; set; }
        public bool isvalid { get; set; }
        public string id { get; set; }
        public bool enabled { get; set; }
        public string username { get; set; }
        public DateTime validto { get; set; }
    }

    public class OppentPayload
    {
        public string sessiontoken { get; set; }
        public OppentUser user { get; set; }
        public OppentAccount account { get; set; }
        public string username { get; set; }
    }

    public class OppentLoginResponse
    {
        public OppentPayload payload { get; set; }
        public int retcode { get; set; }
    }

    public class OppentArgs
    {
        public string value { get; set; }
    }

    public class OppentCommand
    {
        public string name { get; set; }
        public OppentArgs args { get; set; }
    }

    public class OppentWriteAttRequest
    {
        public OppentCommand command { get; set; }
    }




    //public class OppentWriteAttState
    //{

    //    public bool isconnected { get; set; }
    //    public OppentWriteAttInnerState state { get; set; }
    //    public DateTime timestamp { get; set; }
    //}

    //public class OppentWriteAttInnerState
    //{
    //    public int value { get; set; }
    //    public int errCode { get; set; }
    //}

    //public class OppentWriteAttPayload
    //{
    //    public OppentWriteAttState state { get; set; }
    //}

    //public class OppentWriteAttResponse
    //{
    //    public OppentWriteAttPayload payload { get; set; }
    //    public int retcode { get; set; }
    //}

}

