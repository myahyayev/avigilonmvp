﻿using Newtonsoft.Json;
using System;
namespace ConsoleApp1.Models.OppentWriteAtt
{
    public class State2
    {
        public bool isconnected { get; set; }
        public State state { get; set; }
        public DateTime timestamp { get; set; }
    }

    public class Payload
    {
        public State2 state { get; set; }
    }

    public class State
    {
        public int value { get; set; }
    }

    public class OppentWriteAttResponse
    {
        public Payload payload { get; set; }
        public int retcode { get; set; }
    }
}
