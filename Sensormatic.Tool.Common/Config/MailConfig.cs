﻿namespace Sensormatic.Tool.Common
{
    public class MailConfig
    {
        public string SMTPServer { get; set; }
        public string SMTPMailFrom { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }
        public int SMTPPort { get; set; }
    }
}
