﻿namespace Sensormatic.Tool.Common
{
    public static class Queues
    {
        public const string CensusAddDetails = "Census.AddDetails";
        public const string WebServiceLogCommand = "Census.WebServiceLogCommand";
        public const string CensusDetailsConsumerLogCommand = "Census.ConsumerLogCommand";
        
    }
}
