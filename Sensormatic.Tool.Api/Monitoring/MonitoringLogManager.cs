﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace Sensormatic.Tool.Api
{
    public interface IMonitorLogManager
    {
        bool TryCreate(string actionName, IDictionary<string, object> arguments, bool hasServiceLog, out WebServiceLog log);

        void CompleteLog(ActionExecutedContext context, IDictionary<object, object> contextItems, MonitoringResultHandler OnMonitoringResultHandler);
    }

    public class MonitorLogManager : IMonitorLogManager, IScopedDependency
    {
        public bool TryCreate(string actionName, IDictionary<string, object> arguments, bool hasServiceLog, out WebServiceLog log)
        {
            log = new WebServiceLog
            {
                MonitorLog = new MonitorLog
                {
                    Id = Guid.NewGuid(),
                    ActionName = actionName,
                    ActiveDate = DateTime.Now
                },
            };

            if (hasServiceLog)
            {
                //string requestText = JsonSerializer.Serialize(arguments);

                log.ServiceLog = new ServiceLog
                {
                    Id = Guid.NewGuid(),
                    MonitorLogId = log.MonitorLog.Id,
                    Request = arguments,
                    RequestTime = DateTime.Now,
                };
            }

            return true;
        }
        public void CompleteLog(ActionExecutedContext context, IDictionary<object, object> contextItems, MonitoringResultHandler logDispatcher)
        {
            if (bool.TryParse(contextItems["HasServiceLog"]?.ToString(), out bool hasServiceLog))
            {
                if (contextItems["SessionInformation"] is WebServiceLog sessionInformation)
                {
                    //string responseText = "";
                    object data = null;

                    if (hasServiceLog)
                    {
                        var options = new JsonSerializerOptions
                        {
                            WriteIndented = true,
                            PropertyNameCaseInsensitive = true,
                            Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
                        };

                        //responseText = context.Exception switch
                        //{
                        //    BaseException => JsonSerializer.Serialize(((BaseException)context.Exception).ErrorModel, options),
                        //    _ => JsonSerializer.Serialize((context.Result as ObjectResult).Value, options)
                        //};

                        data = context.Exception switch
                        {
                            BaseException => JsonSerializer.Serialize(((BaseException)context.Exception).ErrorModel, options),
                            _ => JsonSerializer.Serialize((context.Result as ObjectResult).Value, options)
                        };

                        sessionInformation.ServiceLog.Response = data;
                        sessionInformation.ServiceLog.ResponseTime = DateTime.Now;
                        sessionInformation.ServiceLog.Duration = (long?)DateTime.Now.Subtract(sessionInformation.ServiceLog.RequestTime).TotalMilliseconds;
                    }

                    logDispatcher?
                        .Invoke(sessionInformation, hasServiceLog);
                }
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
