﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ConsoleApp1.Models.OppentDeviceControl
{
    public class Value
    {
        public bool freevalue { get; set; }
        public bool fixedvalue { get; set; }
        public List<object> domain { get; set; }
    }

    public class Args
    {
        public Value value { get; set; }
    }

    public class Command
    {
        public Args args { get; set; }
        public string name { get; set; }
    }

    public class Commandset
    {
        public string version { get; set; }
        public List<Command> commands { get; set; }
        public string devicetype { get; set; }
    }

    public class State2
    {
        public int value { get; set; }
        public bool isconnected { get; set; }
        public State state { get; set; }
        public DateTime timestamp { get; set; }
    }

    public class State
    {
        public int value { get; set; }
    }

    public class Meta
    {
        public string driverid { get; set; }
        public string hardwareid { get; set; }
        public Commandset commandset { get; set; }
        public State2 state { get; set; }
        public string deviceid { get; set; }
        public string devicetype { get; set; }
    }

    public class Currentnode
    {
        public string name { get; set; }
        public int id { get; set; }
    }

    public class Location
    {
        public List<double> coord { get; set; }
        public int course { get; set; }
        public Currentnode currentnode { get; set; }
        public string map { get; set; }
        public string group { get; set; }
    }

    public class Device
    {
        public string node { get; set; }
        public Meta meta { get; set; }
        public string name { get; set; }
        public Location location { get; set; }
        public string type { get; set; }
        public string @class { get; set; }
        public bool enabled { get; set; }
    }

    public class Payload
    {
        public List<Device> devices { get; set; }
    }

    public class OppentDeviceControlResponse
    {
        public Payload payload { get; set; }
        public int retcode { get; set; }
    }

}
