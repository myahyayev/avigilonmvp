﻿using ConsoleApp1.Models.OppentDeviceControl;
using ConsoleApp1.Models.OppentWriteAtt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ConsoleApp1
{
    class Program
    {
        private static string avgBaseAvgUrl = "https://localhost:8443/mt/api/rest/v1/";
        private static string avgLoginUrl = $"{avgBaseAvgUrl}login";
        private static string avgEventUrl = $"{avgBaseAvgUrl}events/search?";
        private static string avgUserNonce = "0014y00002UeGMgAAN";
        private static string avgUserKey = "18a17222eb6ec766dba0378021dd91d947bf2d371383b0e9f36c653c8438411b";
        private static string avgClientName = "AvigilonAlarmDemoApp.UI";
        private static string avgPassword = "Q1w2e3r4t5"; //"Ex1234xx",
        private static string avgSiteName = "TRKESL0012"; //EXROOM02
        private static string avgUserName = "administrator";
        private static string avgQueryType = AvgQueryType.TIME_RANGE.ToString();
        private static string avgServerId = "gurVyb-9SIazNKh6n70Eqw"; //"H4F-HdtZSf6hdWVvnD_j7Q",
        private static int avgLimit = 1000;


        private static string oppentBaseUrl = "http://localhost:8081/wms/";
        private static string oppentLoginUrl = $"{oppentBaseUrl}monitor/session/login?";
        private static string oppentUserName = "admin";
        private static string oppentPassword = "123456";

        static async Task Main(string[] args)
        {
            while (true)
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                    //Login avg
                    var loginResponse = await Program.AvgLoginAsync();

                    //Get events
                    var eventResponse = await Program.AvgGetEventsAsync(
                                                                        avgEventUrl,
                                                                        loginResponse.result.session,
                                                                        new List<string> { "DEVICE_ANALYTICS_START", "DEVICE_ANALYTICS_STOP" }
                                                                        );

                    Console.WriteLine(eventResponse.result.events.Count);

                    if (eventResponse != null && eventResponse.status == "success")
                    {
                        var grouppedEvents = eventResponse.result.events
                            .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                            .ToList();

                        #region Control
                        foreach (var groupedEvent in grouppedEvents)
                        {
                            Console.WriteLine(groupedEvent.analyticEventName + "  " + groupedEvent.type + "  " + groupedEvent.timestamp);
                        }
                        Console.WriteLine("--------------------------------------------------------------");



                        var groupedEventsOnlyStop = eventResponse.result.events
                            .Where(u => u.type == "DEVICE_ANALYTICS_STOP")
                            .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                            .ToList();

                        foreach (var groupedEvent in groupedEventsOnlyStop)
                        {
                            Console.WriteLine(groupedEvent.analyticEventName + "  " + groupedEvent.type + "  " + groupedEvent.timestamp);
                        }
                        Console.WriteLine("--------------------------------------------------------------");


                        var groupedEventsOnlyStart = eventResponse.result.events
                            .Where(u => u.type == "DEVICE_ANALYTICS_START")
                            .GroupBy(p => p.analyticEventName, (key, g) => g.OrderByDescending(y => y.timestamp).First())
                            .ToList();

                        foreach (var groupedEvent in groupedEventsOnlyStart)
                        {
                            Console.WriteLine(groupedEvent.analyticEventName + "  " + groupedEvent.type + "  " + groupedEvent.timestamp);
                        }

                        Console.WriteLine("--------------------------------------------------------------");

                        #endregion Control


                        var oppentLoginResponse = await Program.OppentLoginAsync();

                        //DEVICE_ANALYTICS_STOP
                        foreach (var e in grouppedEvents.Where(u => u.type == "DEVICE_ANALYTICS_STOP"))
                        {
                            var oppentDeviceControlResponseInput = await Program.OppentDeviceControlAsync($"{oppentBaseUrl}rest/devices/{e.analyticEventName}-input?&sessiontoken={oppentLoginResponse.payload.sessiontoken}", "DEVICE_ANALYTICS_STOP", e.analyticEventName, "input");
                            {
                                if (oppentDeviceControlResponseInput != null && oppentDeviceControlResponseInput.payload != null && oppentDeviceControlResponseInput.payload.devices != null)
                                {
                                    var deviceInput = oppentDeviceControlResponseInput.payload.devices.FirstOrDefault();
                                    if (deviceInput != null && deviceInput.meta.state.state.value == 0)
                                    {
                                        var oppentWriteAttResponse = 
                                            await Program.OppentWriteAttAsync($"{oppentBaseUrl}rest/devices/{e.analyticEventName}-input/command?&sessiontoken={oppentLoginResponse.payload.sessiontoken}", 1, "DEVICE_ANALYTICS_STOP", e.analyticEventName, "input");
                                    }
                                }
                            }                          
                        }


                        //DEVICE_ANALYTICS_START
                        foreach (var e in grouppedEvents.Where(u => u.type == "DEVICE_ANALYTICS_START"))
                        {
                            var oppentDeviceControlResponseOutput = await Program.OppentDeviceControlAsync($"{oppentBaseUrl}rest/devices/{e.analyticEventName}-output?&sessiontoken={oppentLoginResponse.payload.sessiontoken}", "DEVICE_ANALYTICS_START", e.analyticEventName, "output");

                            if (oppentDeviceControlResponseOutput != null && oppentDeviceControlResponseOutput.payload != null && oppentDeviceControlResponseOutput.payload.devices != null)
                            {
                                var deviceOutput = oppentDeviceControlResponseOutput.payload.devices.FirstOrDefault();

                                if (deviceOutput != null && deviceOutput.meta.state.state.value == 0)
                                {
                                    var oppentDeviceControlResponseInput = 
                                        await Program.OppentDeviceControlAsync($"{oppentBaseUrl}rest/devices/{e.analyticEventName}-input?&sessiontoken={oppentLoginResponse.payload.sessiontoken}", "DEVICE_ANALYTICS_START", e.analyticEventName, "input");

                                    if (oppentDeviceControlResponseInput != null && oppentDeviceControlResponseInput.payload != null && oppentDeviceControlResponseInput.payload.devices != null)
                                    {
                                        var deviceInput = oppentDeviceControlResponseInput.payload.devices.FirstOrDefault();
                                        if (deviceInput != null && deviceInput.meta.state.state.value == 1)
                                        {
                                            var oppentWriteAttResponse = 
                                                await Program.OppentWriteAttAsync($"{oppentBaseUrl}rest/devices/{e.analyticEventName}-input/command?&sessiontoken={oppentLoginResponse.payload.sessiontoken}", 0, "DEVICE_ANALYTICS_START", e.analyticEventName, "input");
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "  " + ex.StackTrace);
                }

                Console.WriteLine("Next loop-------------------------------------------------------------------------------------------------------Next loop");
            }
        }

        private static string GenerateAuthToken()
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var timeStamp = (DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() / 1000 | 0).ToString();

            var stringBuilder = new StringBuilder();

            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(timeStamp + avgUserKey));

            foreach (byte theByte in crypto)
                stringBuilder.Append(theByte.ToString("x2"));

            return $"{avgUserNonce}:{ timeStamp}:{ stringBuilder.ToString()}";
        }

        private static async Task<AvgLoginResonse> AvgLoginAsync()
        {
            string authToken = Program.GenerateAuthToken();

            AvgLoginResonse loginResonse;

            var reqesut = new AvgLoginRequest
            {
                authorizationToken = authToken,
                clientName = avgClientName,
                password = avgPassword,
                siteName = avgSiteName,
                username = avgUserName
            };
            using (var client = new HttpClient())
            {
                var httpContent = new StringContent(JsonConvert.SerializeObject(reqesut), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(avgLoginUrl, httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    loginResonse = JsonConvert.DeserializeObject<AvgLoginResonse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return loginResonse;
        }

        private static async Task<AvgEventResponse> AvgGetEventsAsync(string eventUrl, string session, List<string> eventTopincs)
        {
            AvgEventResponse eventResponse;

            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(eventTopincs);
                var builder = new UriBuilder(eventUrl);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["session"] = session;
                query["queryType"] = avgQueryType;
                query["serverId"] = avgServerId;
                query["eventTopics"] = json;
                var now = DateTime.UtcNow;
                query["from"] = now.AddMinutes(-10).ToString("s") + "Z";
                query["to"] = now.ToString("s") + "Z";

                builder.Query = query.ToString();
                string queryUrl = builder.ToString();

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("x-avg-session", session);

                var response = await client.GetAsync(queryUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    eventResponse = JsonConvert.DeserializeObject<AvgEventResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return eventResponse;
        }

        private static async Task<OppentLoginResponse> OppentLoginAsync()
        {
            OppentLoginResponse oppentLoginResponse;

            using (var client = new HttpClient())
            {
                var builder = new UriBuilder(oppentLoginUrl);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["username"] = oppentUserName;
                query["pwd"] = oppentPassword;

                builder.Query = query.ToString();
                string queryUrl = builder.ToString();

                var response = await client.GetAsync(queryUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentLoginResponse = JsonConvert.DeserializeObject<OppentLoginResponse>(result);
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            return oppentLoginResponse;
        }

        private static async Task<OppentWriteAttResponse> OppentWriteAttAsync(string eventUrl, int value, string alarmType, string zoneName, string input)
        {
            OppentWriteAttResponse oppentWriteAttResponse;

            var request = new OppentWriteAttRequest()
            {
                command = new OppentCommand
                {
                    args = new OppentArgs()
                    {
                        value = value.ToString()
                    },
                    name = "write"
                }
            };

            using (var client = new HttpClient())
            {
                var httpContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(eventUrl, httpContent);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    oppentWriteAttResponse = JsonConvert.DeserializeObject<OppentWriteAttResponse>(result);
                
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            if (oppentWriteAttResponse == null || oppentWriteAttResponse.payload == null || oppentWriteAttResponse.payload.state == null || oppentWriteAttResponse.payload.state.state == null)
                Console.WriteLine($"MNethodName: OppentWriteAttAsync,  AlarmType: {alarmType},  ZoneName: {zoneName},  Input: {input},   Result:  value is null");

            else
                Console.WriteLine($"MNethodName: OppentWriteAttAsync,  AlarmType: {alarmType},  ZoneName: {zoneName},  Input: {input},  Result:  {oppentWriteAttResponse.payload.state.state.value}");

            return oppentWriteAttResponse;
        }

        private static async Task<OppentDeviceControlResponse> OppentDeviceControlAsync(string apiUrl,  string alarmType, string zoneName, string inputOrOutput )
        {
            OppentDeviceControlResponse oppentDeviceControlResponse;

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(apiUrl);

                var result = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("OppentDeviceControlAsync Metotun icinde" + result);

                    oppentDeviceControlResponse = JsonConvert.DeserializeObject<OppentDeviceControlResponse>(result);
                }
                else
                    throw new Exception($"{response.StatusCode} : {result} ");
            }

            if (oppentDeviceControlResponse == null || oppentDeviceControlResponse.payload == null || oppentDeviceControlResponse.payload.devices == null || oppentDeviceControlResponse.payload.devices.FirstOrDefault() == null || oppentDeviceControlResponse.payload.devices.FirstOrDefault().meta == null)
            {
                Console.WriteLine($"MNethodName: OppentDeviceControlAsync,  AlarmType: {alarmType},  ZoneName: {zoneName},  Input: {inputOrOutput},   Result:  value is null");
            }
            else
            {
                Console.WriteLine($"MNethodName: OppentDeviceControlAsync,  AlarmType: {alarmType},  ZoneName: {zoneName},  Input: {inputOrOutput},   Result:  value is null");
            }

            return oppentDeviceControlResponse;
        }
    }



    public class AvgLoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string authorizationToken { get; set; }
        public string clientName { get; set; }
        public string siteName { get; set; }
    }

    public class AvgSession
    {
        public string session { get; set; }
        public string externalUserId { get; set; }
        public string domainId { get; set; }
    }

    public class AvgLoginResonse
    {
        public string status { get; set; }
        public AvgSession result { get; set; }
    }

    public enum AvgQueryType
    {
        ACTIVE,
        TIME_RANGE,
        CONTINUE
    }

    public class AvgClassifiedObject
    {
        public string subclass { get; set; }
        public int objectId { get; set; }
    }

    public class AvgEvent
    {
        public List<object> targetIdsDeprecated { get; set; }
        public List<AvgClassifiedObject> classifiedObjects { get; set; }
        public string analyticEventName { get; set; }
        public string area { get; set; }
        public string activity { get; set; }
        public DateTime eventTriggerTime { get; set; }
        public string cameraId { get; set; }
        public string thisId { get; set; }
        public string linkedEventId { get; set; }
        public DateTime timestamp { get; set; }
        public string originatingEventId { get; set; }
        public string originatingServerName { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public string originatingServerId { get; set; }
        public List<object> cameraIds { get; set; }
        public List<object> entityIds { get; set; }
    }

    public class AvgResult
    {
        public List<AvgEvent> events { get; set; }
    }

    public class AvgEventResponse
    {
        public string status { get; set; }
        public AvgResult result { get; set; }
    }



    public class OppentUser
    {
        public string firstname { get; set; }
        public string employeenr { get; set; }
        public string lastname { get; set; }
    }

    public class OppentRole
    {
        public string name { get; set; }
    }

    public class OppentAccount
    {
        public List<OppentRole> roles { get; set; }
        public DateTime validfrom { get; set; }
        public bool isvalid { get; set; }
        public string id { get; set; }
        public bool enabled { get; set; }
        public string username { get; set; }
        public DateTime validto { get; set; }
    }

    public class OppentPayload
    {
        public string sessiontoken { get; set; }
        public OppentUser user { get; set; }
        public OppentAccount account { get; set; }
        public string username { get; set; }
    }

    public class OppentLoginResponse
    {
        public OppentPayload payload { get; set; }
        public int retcode { get; set; }
    }

    public class OppentArgs
    {
        public string value { get; set; }
    }

    public class OppentCommand
    {
        public string name { get; set; }
        public OppentArgs args { get; set; }
    }

    public class OppentWriteAttRequest
    {
        public OppentCommand command { get; set; }
    }

    //"https://192.168.87.65:8443/mt/api/rest/v1/login"
    //"https://localhost:8443/mt/api/rest/v1/login"
    //query["from"] = "2020-01-27T19:00:00.000Z";
    //query["to"] = "2022-01-27T19:00:00.000Z";
    //query["serverId"] = "H4F-HdtZSf6hdWVvnD_j7Q";
}

