﻿namespace NestleAvgOppentConsoleApp.Enums
{
    public enum AvgQueryType
    {
        ACTIVE,
        TIME_RANGE,
        CONTINUE
    }
}
