﻿namespace NestleAvgOppentConsoleApp.Models
{
    public class AvgLoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string authorizationToken { get; set; }
        public string clientName { get; set; }
        public string siteName { get; set; }
    }
}
