﻿using System;

namespace NestleAvgOppentConsoleApp.Models
{
    public class OppentWriteAttResponse
    {
        public OppentAttPayload payload { get; set; }
        public int retcode { get; set; }
    }

    public class OppentWriteAttState2
    {
        public bool isconnected { get; set; }
        public OppentAttState state { get; set; }
        public DateTime timestamp { get; set; }
    }

    public class OppentAttPayload
    {
        public OppentWriteAttState2 state { get; set; }
    }

    public class OppentAttState
    {
        public int value { get; set; }
    }
}
