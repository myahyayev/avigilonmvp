﻿namespace NestleAvgOppentConsoleApp.Models
{
    public class OppentWriteAttRequest
    {
        public OppentCommand command { get; set; }
    }

    public class OppentCommand
    {
        public string name { get; set; }
        public OppentArgs args { get; set; }
    }

    public class OppentArgs
    {
        public string value { get; set; }
    }
}
