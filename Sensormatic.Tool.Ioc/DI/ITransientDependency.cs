﻿using System;

namespace Sensormatic.Tool.Ioc
{
    public interface ITransientDependency : IDisposable { }
}
